# Create your views here.
import logging

from spyne.application import Application
from spyne.protocol.soap import Soap11
from spyne.server.wsgi import WsgiApplication

from spyne.decorator import srpc
from spyne.service import ServiceBase
from spyne.model.complex import Iterable
from spyne.model.primitive import Integer
from spyne.model.primitive import Unicode
from proyecto2.models import Usuario, Sesion
from django.utils.encoding import smart_str, smart_unicode
from django.db import connection

class Proyecto2WebService(ServiceBase):
    @srpc(Integer, Unicode, Unicode, Integer, _returns=Iterable(Iterable(Unicode)))
    def verReporte(codigoReporte, param1, param2):
        tuplas = []
        headers = []
        query = ""
        
        if codigoReporte == 0:  #Cantidad de Equipos en el Mundo
            query = "SELECT max(rownum) \"Total Equipos\" FROM Equipo";
        
        elif codigoReporte == 1: #Pais que mas ligas tiene
            query = "SELECT * FROM (SELECT nombre_pais \"Pais\", count(nombre_liga) as Total_Ligas FROM Liga GROUP BY nombre_pais ORDER BY Total_Ligas DESC) WHERE rownum = 1"
        
        elif codigoReporte == 2: #Promedio de los goles echados en algun minuto.
            query = "SELECT to_char((SELECT count(*) as GOLES_MIN FROM Detalle_Partido WHERE evento_nombre LIKE 'Gol%%' AND evento_min = " + param1 + ") / (SELECT count(*) as GOLES_MIN FROM Detalle_Partido WHERE evento_nombre LIKE 'Gol%%') * 100, '999.999') \"PORCENTAJE\" FROM DUAL"
            
        elif codigoReporte == 3: #Nombrar los Equipos que tiene un estadio.
            query = "select nombre_equipo \"Equipos\" from equipo where nombre_estadio LIKE '" + param1 + "'"; 
            
        elif codigoReporte == 4: #Equipo con mas enfrentamientos.
            query = "select e.nombre_equipo \"Equipo\", count(*) \"Partidos Totales\" from equipo e, partido p where e.nombre_equipo = p.equipo1 or e.nombre_equipo = p.equipo2 group by e.nombre_equipo having count(*) = (select max(count(*)) from equipo e, partido p where e.nombre_equipo = p.equipo1 or e.nombre_equipo = p.equipo2 group by e.nombre_equipo)"
            
        elif codigoReporte == 5: #Cuantos Partidos a ganado determinado equipo.
            query = "select (select count(*) from partido where equipo1 LIKE '" + param1 + "' AND goles_equipo1 > goles_equipo2) + (select count(*) from partido where equipo2 LIKE '" + param1 + "' AND goles_equipo2 > goles_equipo1) \"Partidos Ganados\" from dual"
            
        elif codigoReporte == 6: #Cuantos Partidos a empatado determinado equipo en determinado equipo.
            query = "select fecha_partido \"Fecha\", instancia \"Instancia\", equipo1 \"Equipo\", goles_equipo1 \"G\", equipo2 \"Equipo\", goles_equipo2 \"G\" from partido where ((equipo1 LIKE '" + param1 + "' AND equipo2 LIKE '" + param2 + "') OR (equipo1 LIKE '" + param2 + "' AND equipo2 LIKE '" + param1 + "') ) AND goles_equipo1 = goles_equipo2"
            
        elif codigoReporte == 7: #Que paises no tienen ligas.
            query = "select nombre_pais \"Pais\" from pais MINUS select distinct nombre_pais from liga"
            
        elif codigoReporte == 8: #Que arbitros tienen partidos en la misma fecha.
            query = "select d.id_partido \"Partido\", d.nombre_arbitro \"Arbitro\" from detalle_partido d, partido p where d.id_partido = p.id_partido and p.fecha_partido like to_date('" + param1 + "', 'dd.mm.yyyy') group by d.id_partido, d.nombre_arbitro"
            
        elif codigoReporte == 9: #Arbitro con mas llamados a Principal.
            query = "select nombre_arbitro \"Arbitro\", count(*) \"Convocatorias\" from asignacion_arbitro where rol_arbitro like 'Principal' group by nombre_arbitro having count(*) = (select max(count(*)) as convocatorias from asignacion_arbitro where rol_arbitro like 'Principal' group by nombre_arbitro)"
                
        elif codigoReporte == 10: #Jugador con mas llamados a suplente.
            query = "select nombre_jugador \"Jugador\", count(*) \"Suplencias\" from detalle_alineacion where rol_alineacion like 'Suplente' group by nombre_jugador having count(*) = (select max(count(*)) as suplencias from detalle_alineacion where rol_alineacion like 'Suplente' group by nombre_jugador)"
        
        elif codigoReporte == 11: #Jugador con mas Tarjetas rojas.
            query = "select nombre_jugador \"Jugador\", count(*) \"Expulsiones\" from detalle_partido where evento_nombre LIKE 'Tarjeta Roja' group by nombre_jugador having count(*) = (select max(count(*)) as expulsiones from detalle_partido where evento_nombre LIKE 'Tarjeta Roja' group by nombre_jugador)"
        
        elif codigoReporte == 12: #Jugadores Contratados en el mismo rango de fecha.
            query = "select nombre_jugador \"Jugador\" from contrato where fechainiciacontrato > to_date('" + param1 + "', 'dd.mm.yyyy') AND fechainiciacontrato < to_date('" + param2 + "', 'dd.mm.yyyy')"
        
        elif codigoReporte == 13: #El equipo de cada liga que comete menos falta.
            headers.append("El reporte no esta disponible")
            tuplas.append(headers)
            return tuplas
            
        elif codigoReporte == 14: #Estadio que no ha tenido partidos.
            query = 'SELECT DISTINCT nombre_estadio from estadio MINUS SELECT DISTINCT nombre_estadio from partido where nombre_Estadio is not null;'
        
        elif codigoReporte == 15: #Lista de estadios
            query = "select nombre_estadio \"<Estadio>\" from estadio order by nombre_estadio"
            
        elif codigoReporte == 16: #Lista de equipos
            query = "select nombre_equipo \"<Equipo>\" from equipo order by nombre_equipo"
            
        else:
            headers.append("El reporte no esta disponible")
            tuplas.append(headers)
            return tuplas
            
        cursor = connection.cursor()
        cursor.execute(query)
        
        if 1 <= 0:
            headers.append("No se encontro ningun resultado")
            tuplas.append(headers)
            return tuplas
            
        else:
            dict_columnas = cursor.description
        
            for i in range(0, len(dict_columnas)):
                headers.append(dict_columnas[i][0])
            
            tuplas.append(headers)
        
            for registro in cursor:            
                columnas = []
            
                for i in range(0, len(registro)):
                    columnas.append(smart_unicode(registro[i]))
                
                tuplas.append(columnas)
        
            return tuplas     
            
    @srpc(Unicode, Unicode, Unicode, _returns=Integer)
    def sigUp(user, email, password):
        u = Usuario()
        u.username = user
        u.email = email
        u.passw = password
        
        if Usuario.objects.get(username = user) != 0:
            return -1
            
        else:
            u.save()
            return 0



from wsgiref.simple_server import make_server

logging.basicConfig(level=logging.DEBUG)
logging.getLogger('spyne.protocol.xml').setLevel(logging.DEBUG)

logging.info("El servicio esta publicado en http://darth_vader.com:8001")
logging.info("El archivo descriptor wsdl esta en http://darth_vader.com:8001/?wsdl")

application = Application([Proyecto2WebService], 'http://proyecto2-python.com/soap/',
              in_protocol=Soap11(validator='lxml'),
                out_protocol=Soap11()
          )
wsgi_application = WsgiApplication(application)

server = make_server('darth_vader.com', 8001, wsgi_application)
server.serve_forever()
