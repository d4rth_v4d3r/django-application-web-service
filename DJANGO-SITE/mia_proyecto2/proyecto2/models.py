from django.db import models

# Create your models here.
class Usuario(models.Model):
    username = models.CharField(primary_key=True, max_length=100)
    email = models.CharField(max_length=100, null=False)
    passw = models.CharField(max_length=100, null=False)
    
    def __str__(self):
        return self.username

class Sesion(models.Model):
    username = models.ForeignKey(Usuario)
    fechalogin = models.DateTimeField()
    fechalogout = models.DateTimeField()  
        
    def __str__(self):
        return self.username.username + " : " + self.fechalogin.strftime('%d-%m-%Y %H:%M') + " to " + self.fechalogin.strftime('%d-%m-%Y %H:%M')
