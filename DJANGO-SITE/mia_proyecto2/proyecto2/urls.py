from django.conf.urls import patterns, url

from proyecto2 import views

urlpatterns = patterns('',
    (r'^', 'proyecto2.views.proyecto2ws'),
    (r'^descriptor.wsdl', 'proyecto2.views.proyecto2ws'),
)
