from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'mia_proyecto2.views.home', name='home'),
    # url(r'^mia_proyecto2/', include('mia_proyecto2.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^proyecto2/', include('proyecto2.urls')),
    url(r'^admin/', include(admin.site.urls)),    
)
